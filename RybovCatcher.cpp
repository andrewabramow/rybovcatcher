/*
���������� ������� ����-��������� ����� ����. ������� ����
������������ ����� ������ �������� ��������� �����.
� ������ ���� � ����� �� ���� �������� � �������
���������� ��������� ����� ����������� ����. ����� ��� ��,
��������� �������, �������������� � ��� ������. ����� � 
���� ��� ���� �� ����� ���������� � ����� � ��� �� 
������� ������������, ��� � ��������� ����� �����.

����� ���������� ������ � ��������� � ���� �� ��������, �����
�������� �������� ����������� �������� ����� �������. ������ 
������ � ������� ������, � ������� ��������� ���� �, ����� 
�������, ������� �. ���� ����� ����� �� ������ ������, �� 
������ ���� ��������� ��������. ���� ����� ������ �����, �� 
���� ����������� � ������������� �����������.

����������� ������ ����� � ������� ����������: ����� ��������
������ ������ ���������� ����������� ���������������� 
����������, ����� ���� ��������� ������ ����������� � 
���������� ������������ �� �������� ������� � ���������� 
�������, ������� ��� ��� ����� �������������. ���� �� ��� 
������ �����, �� ������ ���������� ��������� � ��������� 
�����.
*/

#include <iostream>
#include <exception>
#include <ctime>

class FishException : public std::exception {
public:
    const char* what() const noexcept override {
        return "You win!";
    }
};
class BootException : public std::exception {
public:
    const char* what() const noexcept override {
        return "You lose!";
    }
};
class SingleEventUpset : public std::exception {
public:
    const char* what() const noexcept override {
        return "Something went wrong";
    }
};

enum flags {
    EMPTY,
    FISH,
    BOOT
};

int main()
{
    std::srand(std::time(nullptr));
    // this is the pond:
    int pond[3][3]{ 0 };
    // fish placement:
    pond[rand() % 2][rand() % 2] |= FISH;
    // boots placement:
    int counter = 0;
    while (counter != 3) {
        int n = rand() % 2;
        int m = rand() % 2;
        if (pond[n][m] == 0) {
            pond[n][m] |= BOOT;
            counter++;
        }
    }
    // start the game:
    int try_counter = 0;
    try {
        while (counter != 9) { // not endless game
            int n = 0; int m = 0;
            std::cout << "Please input sector"<< std::endl;
            std::cin >> n >> m;
            if (pond[n][m] == 1) {
                std::cout << "Fish!" << std::endl;
                throw FishException();
            }
            else if (pond[n][m] == 2) {
                std::cout << "Boot!" << std::endl;
                throw BootException();
            }
            else if (pond[n][m] == 0) {
                std::cout << "Try again!" << std::endl;
                try_counter++;
            }
            else {
                throw SingleEventUpset();
            }
        }
    }
    // game results:
    catch (const FishException& x) {
        std::cerr<< x.what() <<" with "<<try_counter<<" attempts\n";
    }
    catch (const BootException& x) {
        std::cerr << x.what() << std::endl;
    }
    catch (...) {}
}

